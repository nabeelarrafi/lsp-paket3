<?php

namespace App\Http\Controllers;

use App\Models\Penjualan;
use Illuminate\Http\Request;

class PenjualanController extends Controller
{
    public function index()
    {
        $penjualan = Penjualan::all();
        return view('penjualan.index', [
        'penjualan' => $penjualan
        ]);
    }

    public function create()
    {
        return view(
            'penjualan.create', [
            'user' => User::all() 
        ]);
    }

    public function store(Request $request)
    {
        $request->validate([
            'nonota_jual' => 'required',
            'tgl_jual' => 'required',
            'total_jual' => 'required',
            'id_user' => 'required'
            ]);
            $array = $request->only([
                'nonota_jual',
                'tgl_jual',
                'total_jual',
                'penjualan', 'users'
            ]);
            $distributor = Distributor::create($array);
            return redirect()->route('penjualan.index')
            ->with('success_message', 'Berhasil menambah penjualan
            baru');
    }
}
