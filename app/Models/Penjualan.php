<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Penjualan extends Model
{
    use HasFactory;

    protected $table = 'penjualan';
    protected $fillable = [
        'nonota_jual',
        'tgl_jual',
        'total_jual',
        ];

    public function fdistributor(){
    return $this->belongsTo(Distributor::class, 'id_user', 'id');
            }
}
